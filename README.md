# **KGC** Project &ctdot; KendallCon Game Containers
[![](https://badgen.net/badge/icon/kendallcon%2f?label&icon=gitlab&labelColor=555&color=556)](https://gitlab.com/kendallcon) 
[![](https://badgen.net/badge/icon/kgc%2fproject?label&labelColor=555&color=D97)](https://gitlab.com/kendallcon/kgc/project) 
[![](https://badgen.net/badge/null/V1?label&color=224)](docs/standard.md)

## Container Images
Below is a list of all container images in the project with official releases.  
You may also wish to browse all of `kendallcon/kgc`'s repositories for a more complete list.

| Container | Game/Service | Last Release |
|-----------|--------------|-------------:|
| [`kgc/powbomber`](https://gitlab.com/kendallcon/kgc/powbomber) | **Power Bomberman** | *07/29/2023* |
